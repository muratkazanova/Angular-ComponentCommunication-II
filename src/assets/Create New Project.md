### __How to create a new project in Angular?__

Create a root folder to host your projects:

```
mkdir c:\projects
cd c:\projects
```
Run the Angular CLI command to create a new project:

```
ng new YOURPROJECTNAME --style scss --skip-tests --routing
```

The command above will create a sub "YOURPROJECTNAME" folder under the c:\projects folder and will place all the files Angular needs to serve your angular project.

Navigate to this folder and launch Visual Studio Code in that folder: 

```
cd YOURPROJECTNAME
code .
```

Once the project is launched, start the Terminal windows in VS Code by either using the View > Terminal menu or the keyboard shortcut ```Ctrl + ` ``` keys.

On terminal window,  run the below command to install bootstrap and its companion ng-bootstrap library.

```
npm i bootstrap @ng-bootstrap/ng-bootstrap
```

After the installation is completed, open the angular.json file and find the styles key under the architect key and add the bootstrap style on top of existing styles.css 

```js
"architect: {
  ....
 "styles": [
    "./node_modules/bootstrap/dist/css/bootstrap.min.css",
    "src/styles.scss"
  ],
  ....
}
```

Now your Angular project is ready to serve, you can launch it by running the following command in your terminal window:

```
ng serve -o
```

You can add new components to your Angular project by running the following command:

```
ng g c Home
```

This command will create a HomeComponent under "home" subfolder and it will register the component with angular framework.

### __Working with Class's Sample Project__

Go to Lesson 3 on "bit.ly/intro2ng" URL and click on "Angular Fundamentals Project" link:

it has the instructions to clone the project to your computer.

If you don't have ```GIT``` installed on your computer first install GIT from the web site by following the Setup instructions:

"https://git-scm.com/downloads" 
