import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'color-ref',
  templateUrl: './color-reference-item.component.html',
  styleUrls: ['./color-reference-item.component.scss']
})
export class ColorReferenceItemComponent implements OnInit {
  @Input() color: string;
  constructor() {}

  ngOnInit() {}
}
