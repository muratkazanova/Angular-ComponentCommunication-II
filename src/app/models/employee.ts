export interface IEmployee {
  id: number;
  FirstName: string;
  LastName: string;
  Gender?: string;
  EmailAddress?: string;
  PhoneNumber?: string;
}
