import {
  Component,
  AfterViewInit,
  ViewChild,
  OnInit,
  ElementRef,
  Renderer2
} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'lcps';
  constructor() {}
  ngOnInit() {}
}
