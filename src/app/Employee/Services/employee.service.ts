import { Injectable, Output, EventEmitter } from '@angular/core';
import { Observable, of, BehaviorSubject, empty } from 'rxjs';
import { IEmployee } from 'src/app/models/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  employees: IEmployee[];
  EmployeeSourceChanges = new BehaviorSubject('initial');
  @Output()
  EmployeeFormEvent: EventEmitter<boolean> = new EventEmitter();
  constructor() {
    this.employees = this.employees = [
      {
        id: 1,
        FirstName: 'Ken',
        LastName: 'Sanch',
        Gender: 'M',
        EmailAddress: 'ken0@adventure-works.com',
        PhoneNumber: '697-555-0142'
      },
      {
        id: 2,
        FirstName: 'Terri',
        LastName: 'Duffy',
        Gender: 'F',
        EmailAddress: 'terri0@adventure-works.com',
        PhoneNumber: '819-555-0175'
      },
      {
        id: 3,
        FirstName: 'Roberto',
        LastName: 'Tamburell',
        Gender: 'M',
        EmailAddress: 'roberto0@adventure-works.com',
        PhoneNumber: '212-555-0187'
      }
    ];
  }

  getEmployees(): Observable<IEmployee[]> {
    return of(this.employees);
  }

  newEmployee(value: { FirstName: string; LastName: string }) {
    const ids = this.employees.map(o => o['id']).sort();

    this.employees = [
      ...this.employees,
      {
        id: ids.length ? ids[ids.length - 1] + 1 : 1,
        FirstName: value.FirstName,
        LastName: value.LastName
      }
    ];
    this.EmployeeFormEvent.emit(false);
    this.EmployeeSourceChanges.next('New Employee');
  }

  updateEmployee(FirstName, LastName, Id) {
    const employee = this.employees.find(e => e.id === Id);
    employee.FirstName = FirstName;
    employee.LastName = LastName;
    this.EmployeeSourceChanges.next('Employee Updated');
    return of(null);
  }

  deleteEmployee(id: number) {
    this.employees = this.employees.filter(e => e.id != id);
    this.EmployeeSourceChanges.next('Employee Deleted');
    return of(null);
  }
}
