import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EmployeeService } from '../../Services/employee.service';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.scss']
})
export class EmployeeFormComponent implements OnInit {
  constructor(private employeeService: EmployeeService) {}

  ngOnInit() {}

  onNewEmployee(FirstName: string, LastName: string) {
    this.employeeService.newEmployee({ FirstName, LastName });
  }

  onCancel() {
    this.employeeService.EmployeeFormEvent.emit(false);
  }
}
