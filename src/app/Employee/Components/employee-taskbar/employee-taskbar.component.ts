import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EmployeeService } from '../../Services/employee.service';
import { IEmployee } from 'src/app/models/employee';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-employee-taskbar',
  templateUrl: './employee-taskbar.component.html',
  styleUrls: ['./employee-taskbar.component.scss']
})
export class EmployeeTaskbarComponent implements OnInit {
  employees: IEmployee[];

  constructor(private employeeService: EmployeeService) {}

  ngOnInit() {
    this.employeeService.EmployeeSourceChanges.pipe(
      switchMap(_ => this.employeeService.getEmployees())
    ).subscribe((data: IEmployee[]) => {
      this.employees = data;
    });
  }
  onNewEmployee() {
    this.employeeService.EmployeeFormEvent.emit(true);
  }
}
