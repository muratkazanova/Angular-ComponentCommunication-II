import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {
  EmployeeDashboardComponent,
  EmployeeTaskbarComponent,
  EmployeeFormComponent,
  EmployeeItemComponent,
  EmployeeListComponent
} from './Employee/index';
import { ColorReferenceItemComponent } from './Shared/color-reference-item/color-reference-item.component';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeDashboardComponent,
    EmployeeTaskbarComponent,
    EmployeeListComponent,
    EmployeeItemComponent,
    EmployeeFormComponent,
    ColorReferenceItemComponent
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
